//
//  RickyButton.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 11/01/2016.
//
//

#include "RickyButton.h"

RickyButton::RickyButton()
{
    addAndMakeVisible(newButton);
    //newButton.setColour(1, Colours::hotpink);
    
   
    
}

RickyButton::~RickyButton()
{
    
}

void RickyButton::paint(Graphics& g)
{
    //g.setColour(Colours::hotpink);
    newButton.setColour(0x1000100, Colours::hotpink);
    
    //int halfWidth = getWidth()/2.0;
    //int halfHeight = getHeight()/2.0;
    //g.fillRect(0, 0, halfWidth, halfHeight);
}

void RickyButton::resized()
{
    int x = getWidth();
    int y = getHeight();
    newButton.setBounds(0, 0, x/5-2, y/2);
    
}
