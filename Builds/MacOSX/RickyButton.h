//
//  RickyButton.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 11/01/2016.
//
//

#ifndef __JuceBasicWindow__RickyButton__
#define __JuceBasicWindow__RickyButton__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class RickyButton : public Component
{
public:
    RickyButton();
    ~RickyButton();
    
    void paint(Graphics& g) override;
    void resized();
    

    
    
    
    
    
    
private:
    TextButton newButton;
    

};

#endif /* defined(__JuceBasicWindow__RickyButton__) */
