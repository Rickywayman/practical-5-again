//
//  ButtonRow.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 11/01/2016.
//
//

#ifndef __JuceBasicWindow__ButtonRow__
#define __JuceBasicWindow__ButtonRow__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "RickyButton.h"

class ButtonRow : public Component
{
public:
    ButtonRow();
    ~ButtonRow();
    
    void resized() override;
    
    
    
    
    
    
    
private:
    RickyButton button1;
    RickyButton button2;
    RickyButton button3;
    RickyButton button4;
    RickyButton button5;
    
    
};

#endif /* defined(__JuceBasicWindow__ButtonRow__) */
