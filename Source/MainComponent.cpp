/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //addAndMakeVisible(newButton);
    addAndMakeVisible(newRow);
    
    
    
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    int x = getWidth();
    int y = getHeight();
    //newButton.setBounds(100, 100, x, y);
    newRow.setBounds(10, 10, x, y);
}
